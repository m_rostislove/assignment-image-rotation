//
// Created by Rostislav on 28.12.2021.
//

#ifndef CLIONPROJECTS_BMP_H
#define CLIONPROJECTS_BMP_H

#include  <stdint.h>

#include "file_manager.h"
#include "image.h"
#include "util.h"

struct __attribute__((packed)) bmp_header{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status{
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_WIDTH,
    READ_INVALID_HEIGHT,
    READ_DATA_ERROR
};

enum read_status from_bmp( FILE* in, struct image* image );

void read_status_message(enum read_status readStatus );

struct image image_from_bmp(const char* file_name);

enum write_status{
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image const* image );

void write_status_message(enum write_status writeStatus );

void image_to_bmp(struct image img, const char* filename );

#endif //CLIONPROJECTS_BMP_H
