//
// Created by Rostislav on 30.12.2021.
//

#ifndef CLIONPROJECTS_UTIL_H
#define CLIONPROJECTS_UTIL_H

_Noreturn void err( const char* msg, ... );

#endif //CLIONPROJECTS_UTIL_H
