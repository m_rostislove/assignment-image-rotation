//
// Created by Rostislav on 28.12.2021.
//

#ifndef CLIONPROJECTS_IMAGE_H
#define CLIONPROJECTS_IMAGE_H

#include <stdint.h>
#include <malloc.h>

struct __attribute__((packed)) pixel{ uint8_t b, g, r; };

struct image{
    uint64_t width, height;
    struct pixel* data;
};

struct pixel* pixel_get(struct image const image, uint32_t i, uint32_t j );

struct image image_create( uint32_t width, uint32_t height );

void image_destroy(struct image* image );

#endif //CLIONPROJECTS_IMAGE_H
