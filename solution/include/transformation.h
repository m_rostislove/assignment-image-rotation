//
// Created by Rostislav on 28.12.2021.
//

#ifndef CLIONPROJECTS_TRANSFORMATION_H
#define CLIONPROJECTS_TRANSFORMATION_H

#include "image.h"

struct image rotate(struct image const* source, struct image output);

struct image transformation(struct image const* source, struct image f(struct image const*, struct image));

#endif //CLIONPROJECTS_TRANSFORMATION_H
