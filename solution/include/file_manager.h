//
// Created by Rostislav on 24.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_MANAGER_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_MANAGER_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

enum open_status{
    OPEN_OK = 0,
    OPEN_MODE_ERROR,
    OPEN_ERROR
};

enum open_status open_file(FILE **file, const char *file_name, char *mode );

void open_status_message(enum open_status openStatus );

enum close_status{
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum close_status close_file( FILE* file );

void close_status_message(enum close_status closeStatus );

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_MANAGER_H
