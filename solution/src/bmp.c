//
// Created by Rostislav on 29.12.2021.
//

#include "bmp.h"


static uint32_t padding_get(uint32_t width) {return (4 - ((width * sizeof(struct pixel)) % 4));}

static struct bmp_header header_create(uint32_t width, uint32_t height) {
    const uint32_t padding = padding_get(width);
    struct bmp_header header;
    header.bfType = 0x4D42;
    header.biSizeImage = width * height * sizeof(struct pixel) + padding * height;
    header.bOffBits = sizeof(struct bmp_header);
    header.bfileSize = header.biSizeImage + header.bOffBits;
    header.bfReserved = 0;
    header.biSize = 40;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    return header;
}

static const char* const from_bmp_array[] = {
        [READ_OK] = "The file was read successfully\n",
        [READ_INVALID_SIGNATURE] = "Invalid signature\n",
        [READ_INVALID_BITS] = "Invalid bits\n",
        [READ_INVALID_HEADER] = "Invalid header\n",
        [READ_INVALID_WIDTH] = "Invalid width\n",
        [READ_INVALID_HEIGHT] = "Invalid height\n",
        [READ_DATA_ERROR] = "Failed at reading data\n"
};

static const char* const to_bmp_array[] = {
        [WRITE_OK] = "The file was written successfully\n",
        [WRITE_ERROR] = "Failed at writing\n"
};


struct bmp_header header = {0};

static enum read_status header_read( FILE* in ){
    if(fread(&header, sizeof(struct bmp_header), 1, in) != 1){
        return READ_INVALID_HEADER;
    }
    if (header.bfType != 0x4D42)
        return READ_INVALID_SIGNATURE;
    if (header.biBitCount != 24)
        return READ_INVALID_BITS;
    if (header.biWidth <= 0)
        return READ_INVALID_WIDTH;
    if (header.biHeight <= 0)
        return READ_INVALID_HEIGHT;
    return READ_OK;
}

enum read_status from_bmp( FILE* in, struct image* image ){
    if(header_read(in))
        return header_read(in);
    uint32_t h = header.biHeight;
    uint32_t w = header.biWidth;
    uint32_t padding = padding_get(w);
    *image = image_create(w, h);
//    pixel* pixels_row = malloc(sizeof(pixel) * w * h);
//    for (uint32_t i = 0; i < h; i++) {
//        image->data[i] = pixels_row + i * w;
//    }
    for(uint32_t i = 0; i < h; i++){
        if (fread(image->data + i * w, sizeof(struct pixel), w, in) != w || fseek(in, padding, SEEK_CUR) != 0){
            return READ_DATA_ERROR;
        }

    }
    return READ_OK;
}

void read_status_message(enum read_status readStatus ){
    if (readStatus == READ_OK)
        fprintf(stderr,"%s", from_bmp_array[readStatus]);
    else
        err(from_bmp_array[readStatus]);
}

struct image image_from_bmp(const char* file_name){
    FILE *input = {0};
    open_status_message(open_file(&input, file_name, "rb"));
    struct image source = {0};
    read_status_message(from_bmp(input, &source));
    close_status_message(close_file(input));
    return source;
}

enum write_status to_bmp( FILE* out,struct image const* image ) {
    uint64_t width = image->width;
    uint64_t height = image->height;
    struct bmp_header header = header_create(width, height);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }
    uint32_t padding = padding_get(width);
    const uint64_t zeroPadding = 0;
    for (uint32_t i = 0; i < height; i++) {
        if (fwrite(&(image->data)[i * width], sizeof(struct pixel), width, out) != width || fwrite(&zeroPadding, 1, padding, out) != padding) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

void write_status_message(enum write_status writeStatus ){
    if (writeStatus == WRITE_OK)
        fprintf(stderr,"%s", from_bmp_array[writeStatus]);
    else
        err(to_bmp_array[writeStatus]);
}

void image_to_bmp(struct image img, const char* filename){
    FILE *output  = {0};
    open_status_message(open_file(&output, filename, "wb"));
    write_status_message(to_bmp(output, &img));
    close_status_message(close_file(output));
}
