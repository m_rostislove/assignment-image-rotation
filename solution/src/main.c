
#include "bmp.h"
#include "transformation.h"


int main( int argc, char** argv ){
    if (argc < 3) err("Not enough arguments\n");
    if (argc > 3) err("Too many arguments\n");

    struct image source = image_from_bmp(argv[1]);
    struct image rotated = transformation(&source, rotate);
    image_to_bmp(rotated, argv[2]);
    image_destroy(&rotated);
    image_destroy(&source);

    return 0;
}
