//
// Created by Rostislav on 28.12.2021.
//

#include "image.h"

struct image image_create( uint32_t width, uint32_t height ) {
    return (struct image) { .width = width, .height = height, .data = malloc(sizeof(struct pixel) * height * width)};
}

struct pixel* pixel_get(struct image const image, uint32_t i, uint32_t j) {
    return &(image.data[j * image.width + i]);
}

void image_destroy(struct image* image ){
    free(image -> data);
}
