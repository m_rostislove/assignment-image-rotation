//
// Created by Rostislav on 28.12.2021.
//

#include "transformation.h"

struct image rotate(struct image const* source, struct image output ){
    uint32_t counter = 0;
    for (int32_t i = output.width - 1; i >= 0; i--) {
        for (int32_t j = 0; j < output.height; j++) {
            struct pixel* pixel = pixel_get(output, i, j);
            *pixel = source->data[counter];
            ++counter;
        }
    }
    return output;
}

struct image transformation(struct image const* source, struct image f(struct image const*, struct image)){
    struct image output = image_create(source->height, source->width);
    return f(source, output);
}

