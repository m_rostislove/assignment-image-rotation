//
// Created by Rostislav on 24.01.2022.
//
#include "file_manager.h"

static const char* const open_file_array[] = {
        [OPEN_OK] = "The file was opened successfully\n",
        [OPEN_MODE_ERROR] = "Wrong mode to open file\n",
        [OPEN_ERROR] = "Failed at opening file\n"
};

static const char* const close_file_array[] = {
        [CLOSE_OK] = "The file was closed successfully\n",
        [CLOSE_ERROR] = "Failed at closing file\n"
};

enum open_status open_file(FILE** file, const char *file_name, char *mode){
    if (strcmp(mode, "rb") != 0  && strcmp(mode, "wb") != 0)
        return OPEN_MODE_ERROR;
    *file = fopen(file_name, mode);
    if (!file) {
        fprintf(stderr, "%s\n", file_name);
        perror("fopen() ");
        return OPEN_ERROR;
    }
    return OPEN_OK;
}

void open_status_message(enum open_status openStatus ){
    if(openStatus == OPEN_OK)
        fprintf(stderr,"%s", open_file_array[openStatus]);
    else
        err(open_file_array[openStatus]);
}

enum close_status close_file( FILE* file ){
    if (fclose(file) != 0) {
        return CLOSE_ERROR;
    }
    return CLOSE_OK;
}

void close_status_message(enum close_status closeStatus ){
    if(closeStatus == CLOSE_OK)
        fprintf(stderr,"%s", close_file_array[closeStatus]);
    else
        err(close_file_array[closeStatus]);
}
